﻿using DPE.LabChem.Identity.Users;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DPE.LabChem.Identity.Configuration
{
	public class ApplicationSigninManager : SignInManager<ApplicationUser, string>
	{
		public ApplicationSigninManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
			: base(userManager, authenticationManager)
		{
		}

		public override Task<ClaimsIdentity> CreateUserIdentityAsync(ApplicationUser user)
		{
			return user.GenerateUserIdentityAsync((ApplicationUserManager)UserManager);
		}

		public static ApplicationSigninManager Create(IdentityFactoryOptions<ApplicationSigninManager> options, IOwinContext context)
		{
			return new ApplicationSigninManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
		}
	}
}
