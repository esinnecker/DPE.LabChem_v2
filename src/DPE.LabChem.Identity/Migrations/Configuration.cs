using DPE.LabChem.Identity.Context;
using DPE.LabChem.Identity.Users;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Data.Entity.Migrations;
using System.Linq;

namespace DPE.LabChem.Identity.Migrations
{


	internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            const string fullName = "System Administrator";
            const string userName = "Admin";
            const string email = "esinnecker@hotmail.com";
            const string password = "Admin@123";
            const string roleName = "Admin";
            DateTime dateOfBirth = Convert.ToDateTime("2000-01-01");

            if (!context.Roles.Any(r=>r.Name == roleName))
			{
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var role = new IdentityRole
                {
                    Name = roleName
                };

                manager.Create(role);
			}

            if (!context.Users.Any(u=>u.UserName == userName))
			{
                var store = new UserStore<ApplicationUser>(context);
                var manager = new UserManager<ApplicationUser>(store);
                var user = new ApplicationUser
                {
                    FullName = fullName,
                    UserName = userName,
                    Email = email,
                    DateOfBirth = dateOfBirth
                };

                manager.Create(user, password);
                manager.AddToRole(user.Id, roleName);
			}
            base.Seed(context);            
        }
    }
}
