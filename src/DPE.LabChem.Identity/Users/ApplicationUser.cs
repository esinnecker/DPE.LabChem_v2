﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DPE.LabChem.Identity.Users
{
	public class ApplicationUser : IdentityUser
	{
		[Required(ErrorMessage = "Full Name is required!")]
		[MaxLength(120, ErrorMessage = "Max character allowed: 120")]
		[Display(Name = "Full Name")]
		public string FullName { get; set; }

		[Required(ErrorMessage = "Date of Birth is required!")]
		[Display(Name = "Date of Birth")]
		[DataType(DataType.Date)]
		public DateTime DateOfBirth { get; set; }

		[MaxLength(150, ErrorMessage = "Max character allowed: 150")]
		[Display(Name = "Skype")]
		public string Skype { get; set; }		

		[Display(Name = "Bio")]
		public string Bio { get; set; }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="manager"></param>
		/// <returns></returns>
		public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
		{
			// Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
			var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
			// Add custom user claims here
			return userIdentity;
		}
	}
}
