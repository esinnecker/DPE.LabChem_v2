﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DPE.LabChem.Identity.Users
{
	public class LoginViewModel
	{
		[Required]		
		[Display(Name = "User Name")]
		[MaxLength(16, ErrorMessage = "Max character allowed: 16")]
		public string UserName { get; set; }

		[Required(ErrorMessage = "Password is required!")]
		[DataType(DataType.Password)]
		[Display(Name = "Password")]
		public string Password { get; set; }

		[Display(Name = "Remember me?")]
		public bool RememberMe { get; set; }
	}
}
