﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DPE.LabChem.Identity.Users
{
	public class RegisterViewModel
	{
		[Key]
		public string Id { get; set; }

		[Required(ErrorMessage = "User Name is required!")]
		[MaxLength(60, ErrorMessage = "Max character allowed: 60")]
		[Display(Name = "User Name")]
		public string UserName { get; set; }

		[Required(ErrorMessage = "Full Name is required!")]
		[MaxLength(120, ErrorMessage = "Max character allowed: 120")]
		[Display(Name = "Full Name")]
		public string FullName { get; set; }

		[Required(ErrorMessage = "Date of Birth is required!")]
		[Display(Name = "Date of Birth")]
		[DataType(DataType.Date)]
		public DateTime DateOfBirth { get; set; }

		[MaxLength(150, ErrorMessage = "Max character allowed: 150")]
		[Display(Name = "Skype")]
		public string Skype { get; set; }
		
		[Display(Name = "Bio")]
		public string Bio { get; set; }

		[Required(ErrorMessage = "Email is required!")]
		[EmailAddress]
		[Display(Name = "Email")]
		public string Email { get; set; }

		[Required(ErrorMessage = "Password is required!")]
		[StringLength(100, ErrorMessage = "The {0} need to have at least {2} characteres.", MinimumLength = 6)]
		[DataType(DataType.Password)]
		[Display(Name = "Password")]
		public string Password { get; set; }

		[DataType(DataType.Password)]
		[Display(Name = "Confirm Password")]
		[Compare("Password", ErrorMessage = "The password and the password confirmation does not match.")]
		public string ConfirmPassword { get; set; }
	}
}
