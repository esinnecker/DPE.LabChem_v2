﻿using DPE.LabChem.Identity.Users;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace DPE.LabChem.Identity.Context
{
	public class ApplicationDbContext : IdentityDbContext<ApplicationUser>, IDisposable
	{
		public ApplicationDbContext() 
			:base("connApplicationDbContext")
		{
					
		}

		public static ApplicationDbContext Create()
		{
			return new ApplicationDbContext();
		}		

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);
		}		
	}
}
