﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DPE.LabChem.Domain.Entities
{
	public class Country
	{
		public int Id { get; set; }
		[Required(ErrorMessage = "Initial is required!")]
		public string Initials { get; set; }
		[Required(ErrorMessage = "Name is required!")]
		public string Name { get; set; }		
		public virtual IEnumerable<City> City { get; set; }
	}
}
