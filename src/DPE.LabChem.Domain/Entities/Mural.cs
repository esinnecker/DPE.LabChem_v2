﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;

namespace DPE.LabChem.Domain.Entities
{
	public class Mural
	{
		public Mural()
		{
			this.Date = DateTime.Now;

			if (string.IsNullOrEmpty(this.Title))
			{
				this.Title = "Requesting new Credential";
				this.Subject = "Hi! Could you provide a new credential in the system, for e-mail:\n\n ";
			}
		}
		[Key]
		public int Id { get; set; }

		[DataType(DataType.Date, ErrorMessage = "Date is invalid")]
		[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
		public DateTime Date { get; set; }

		[Display(Name = "PostIt Title")]
		[Required(ErrorMessage = "Title is required")]
		[MaxLength(30, ErrorMessage = "Max character allowed: 30")]
		[MinLength(5, ErrorMessage = "Min character allowed: 5")]
		public string Title { get; set; }

		[Display(Name = "Subject")]
		[Required(ErrorMessage = "Subject is required")]
		[MaxLength(335, ErrorMessage = "Max character allowed: 335")]
		[MinLength(3, ErrorMessage = "Min character allowed: 3")]
		public string Subject { get; set; }

		[Required(ErrorMessage = "Email is required")]
		[MaxLength(28, ErrorMessage = "Máximo de caractere permitido: 200")]
		public string Email { get; set; }

		[Required(ErrorMessage = "User is required")]
		[MaxLength(28, ErrorMessage = "Max character allowed: 28")]
		[MinLength(2, ErrorMessage = "Min character allowed: 2")]
		public string User { get; set; }
	}
}
