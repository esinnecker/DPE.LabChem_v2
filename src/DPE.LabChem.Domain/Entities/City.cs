﻿using System.ComponentModel.DataAnnotations;

namespace DPE.LabChem.Domain.Entities
{
	public class City
	{
		public int Id { get; set; }
		[Required]
		public string Name { get; set; }
		[Required]
		public int CountryId { get; set; }
		[Required]
		public virtual Country Country { get; set; }
	}
}
