﻿using System;
using System.Data.Entity.ModelConfiguration;

namespace DPE.Commom.Entity
{
	public abstract class DPEEntityAbstractConfig<TEntity> : EntityTypeConfiguration<TEntity>
		where TEntity : class
	{
		public DPEEntityAbstractConfig()
		{
			ConfigTable();
			ConfigFields();
			ConfigPrimaryKey();
			ConfigForeingKey();
		}

		protected abstract void ConfigTable();

		protected abstract void ConfigFields();

		protected abstract void ConfigPrimaryKey();

		protected abstract void ConfigForeingKey();
	}
}
