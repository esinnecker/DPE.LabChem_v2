﻿using DPE.Commom.Entity;
using DPE.LabChem.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;

namespace DPE.LabChem.Infra.TypeConfiguration
{
	public class CountryTypeConfiguration: DPEEntityAbstractConfig<Country>
	{
		protected override void ConfigFields()
		{
			Property(pk => pk.Id)
				.HasColumnName("Id")
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

			Property(p => p.Initials)
				.IsRequired()
				.HasMaxLength(3).IsFixedLength()
				.HasColumnName("Initials");

			Property(p => p.Name)
				.IsRequired()
				.HasMaxLength(25)
				.HasColumnName("Name");
		}

		protected override void ConfigForeingKey()
		{
			
		}

		protected override void ConfigPrimaryKey()
		{
			HasKey(pk => pk.Id);
		}

		protected override void ConfigTable()
		{
			ToTable("TBL_Country");
		}
	}
}
