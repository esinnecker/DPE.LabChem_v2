﻿using DPE.Commom.Entity;
using DPE.LabChem.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DPE.LabChem.Infra.TypeConfiguration
{
	public class CityTypeConfiguration : DPEEntityAbstractConfig<City>
	{
		protected override void ConfigFields()
		{
			Property(p => p.Name)
				.IsRequired()
				.HasMaxLength(25)
				.HasColumnName("Name");

			Property(p => p.CountryId)
				.IsRequired()
				.HasColumnName("CountryId");
		}

		protected override void ConfigForeingKey()
		{
			HasRequired(rel => rel.Country)
				.WithMany()
				.HasForeignKey(fk => fk.CountryId)
				.WillCascadeOnDelete(false);
		}

		protected override void ConfigPrimaryKey()
		{
			HasKey(pk => pk.Id);
		}

		protected override void ConfigTable()
		{
			ToTable("TBL_City");
		}
	}
}
