namespace DPE.LabChem.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMutalTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TBL_Mural",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Title = c.String(nullable: false, maxLength: 30, unicode: false),
                        Subject = c.String(nullable: false, maxLength: 335, unicode: false),
                        Email = c.String(nullable: false, maxLength: 28, unicode: false),
                        User = c.String(nullable: false, maxLength: 28, unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TBL_Mural");
        }
    }
}
