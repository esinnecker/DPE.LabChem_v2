namespace DPE.LabChem.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.TBL_County", "CountryId", "dbo.TBL_Country");
            //DropForeignKey("dbo.TBL_City", "CountyId", "dbo.TBL_County");
            DropForeignKey("dbo.TBL_CIty", "FK_dbo.City_dbo.County_CountyId");
            DropIndex("dbo.TBL_City", new[] { "CountyId" });
            DropIndex("dbo.TBL_County", new[] { "CountryId" });
            AddColumn("dbo.TBL_City", "CountryId", c => c.Int(nullable: false));
            CreateIndex("dbo.TBL_City", "CountryId");
            AddForeignKey("dbo.TBL_City", "CountryId", "dbo.TBL_Country", "Id");
            DropColumn("dbo.TBL_City", "CountyId");
            DropTable("dbo.TBL_County");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.TBL_County",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 25, unicode: false),
                        CountryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.TBL_City", "CountyId", c => c.Int(nullable: false));
            DropForeignKey("dbo.TBL_City", "CountryId", "dbo.TBL_Country");
            DropIndex("dbo.TBL_City", new[] { "CountryId" });
            DropColumn("dbo.TBL_City", "CountryId");
            CreateIndex("dbo.TBL_County", "CountryId");
            CreateIndex("dbo.TBL_City", "CountyId");
            AddForeignKey("dbo.TBL_City", "CountyId", "dbo.TBL_County", "Id");
            AddForeignKey("dbo.TBL_County", "CountryId", "dbo.TBL_Country", "Id");
        }
    }
}
