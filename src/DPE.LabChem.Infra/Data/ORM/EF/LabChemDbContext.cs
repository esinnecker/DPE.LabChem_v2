﻿using DPE.LabChem.Domain.Entities;
using DPE.LabChem.Infra.Entity;
using DPE.LabChem.Infra.TypeConfiguration;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace DPE.LabChem.Infra.Data.ORM.EF
{
	public class LabChemDbContext : DbContext
	{
		public LabChemDbContext() : base("connLabChemDbContext")
		{
		}

		public DbSet<Country> Country { get; set; }
		public DbSet<City> City { get; set; }
		public DbSet<Mural> Mural { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			//Removendo convencoes
			modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
			modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
			modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

			//E003 - Add FluentAPI Map
			modelBuilder.Configurations.Add(new CountryMap());			
			modelBuilder.Configurations.Add(new CityMap());
			modelBuilder.Configurations.Add(new MuralMap());
			//E003 - Add FluentAPI Map - END

			//E004 - Add Design Pattern Template Method
			//modelBuilder.Configurations.Add(new CountryTypeConfiguration());			
			//modelBuilder.Configurations.Add(new CityTypeConfiguration());
			//E004 - Add Design Pattern Template Method - END

			modelBuilder.Properties()
				.Where(p => p.Name == p.ReflectedType.Name + "Id")
				.Configure(p => p.IsKey());

			modelBuilder.Properties<string>()
				.Configure(p => p.HasColumnType("varchar"));

			modelBuilder.Properties<string>()
				.Configure(p => p.HasMaxLength(80));

			base.OnModelCreating(modelBuilder);
		}
		
	}
}
