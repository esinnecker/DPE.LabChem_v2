﻿using DPE.LabChem.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DPE.LabChem.Infra.Entity
{
	public class MuralMap : EntityTypeConfiguration<Mural>
	{
		public MuralMap()
		{
			ToTable("TBL_Mural");

			//Primary Key
			HasKey(pk => pk.Id);

			//Properties
			Property(p => p.Date)
				.IsRequired()
				.HasColumnName("Date");

			Property(p => p.Title)
				.IsRequired()
				.HasColumnName("Title");

			Property(p => p.Subject)
				.IsRequired()
				.HasColumnName("Subject");

			Property(p => p.Email)
				.IsRequired()
				.HasColumnName("Email");

			Property(p => p.User)
				.IsRequired()
				.HasColumnName("User");
		}
	}
}
