﻿using DPE.LabChem.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace DPE.LabChem.Infra.Entity
{
	public class CountryMap : EntityTypeConfiguration<Country>
	{
		public CountryMap()
		{
			ToTable("TBL_Country");

			//PrimaryKey
			HasKey(pk => pk.Id);

			//Properties
			Property(p => p.Initials)
				.IsRequired()
				.HasMaxLength(3).IsFixedLength()
				.HasColumnName("Initials");

			Property(p => p.Name)
				.IsRequired()
				.HasMaxLength(25)
				.HasColumnName("Name");
		}
		
	}
}
