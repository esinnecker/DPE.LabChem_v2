﻿using DPE.LabChem.Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace DPE.LabChem.Infra.Entity
{
	public class CityMap : EntityTypeConfiguration<City>
	{
		public CityMap()
		{
			ToTable("TBL_City");

			//Primary Key
			HasKey(pk => pk.Id);

			//Properties
			Property(p => p.Name)
				.IsRequired()
				.HasMaxLength(25)
				.HasColumnName("Name");

			Property(p => p.CountryId)
				.IsRequired()
				.HasColumnName("CountryId");

			HasRequired(rel => rel.Country)
				.WithMany()
				.HasForeignKey(fk => fk.CountryId)
				.WillCascadeOnDelete(false);
		}
	}
}
