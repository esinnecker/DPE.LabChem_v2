﻿using System.Web;
using System.Web.Optimization;

namespace DPE.LabChem.Web
{
	public class BundleConfig
	{
		// For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
		public static void RegisterBundles(BundleCollection bundles)
		{
			bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
						"~/Scripts/jquery-{version}.js"));

			bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
						"~/Scripts/jquery.validate*"));

			// Use the development version of Modernizr to develop with and learn from. Then, when you're
			// ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
			bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
						"~/Scripts/modernizr-*"));

			bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
					  "~/Scripts/bootstrap.js",
					  "~/Scripts/respond.js"));

			bundles.Add(new StyleBundle("~/Content/css").Include(
					  "~/Content/bootstrap.css",
					  "~/Content/site.css"));

			bundles.Add(new StyleBundle("~/Template/Styles").Include(
				"~/Template/vendors/datatables.net/css/jquery.dataTables.min.css",
				"~/Template/vendors/bootstrap/dist/css/bootstrap.min.css",
				"~/Template/vendors/font-awesome/css/font-awesome.min.css",
				"~/Template/vendors/nprogress/nprogress.css",
				"~/Template/vendors/iCheck/skins/flat/green.css",
				"~/Template/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css",
				"~/Template/vendors/jqvmap/dist/jqvmap.min.css",
				"~/Template/vendors/bootstrap-daterangepicker/daterangepicker.css",
				"~/Template/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css",
				"~/Template/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css",
				"~/Template/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css",
				"~/Template/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css",
				"~/Template/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css",
				"~/Template/vendors/animate.css/animate.min.css",
				"~/Template/build/css/custom.min.css",
				"~/Content/toastr.css",
				"~/Content/toastr.min.css"
				));

			bundles.Add(new StyleBundle("~/TemplateLogin/Styles").Include(
				"~/Template/vendors/bootstrap/dist/css/bootstrap.min.css", 
				"~/Template/vendors/font-awesome/css/font-awesome.min.css", 
				"~/Template/vendors/nprogress/nprogress.css", 
				"~/Template/vendors/animate.css/animate.min.css",
				"~/Template/build/css/custom.min.css"));

			bundles.Add(new ScriptBundle("~/Template/Scripts").Include(
				"~/Template/vendors/jquery/dist/jquery.min.js",
				"~/Template/vendors/bootstrap/dist/js/bootstrap.bundle.min.js",
				"~/Template/vendors/fastclick/lib/fastclick.js",
				"~/Template/vendors/nprogress/nprogress.js",
				"~/Template/vendors/Chart.js/dist/Chart.min.js",
				"~/Template/vendors/gauge.js/dist/gauge.min.js",
				"~/Template/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js",
				"~/Template/vendors/iCheck/icheck.min.js",
				"~/Template/vendors/skycons/skycons.js",
				"~/Template/vendors/Flot/jquery.flot.js",
				"~/Template/vendors/Flot/jquery.flot.pie.js",
				"~/Template/vendors/Flot/jquery.flot.time.js",
				"~/Template/vendors/Flot/jquery.flot.stack.js",
				"~/Template/vendors/Flot/jquery.flot.resize.js",
				"~/Template/vendors/flot.orderbars/js/jquery.flot.orderBars.js",
				"~/Template/vendors/flot-spline/js/jquery.flot.spline.min.js",
				"~/Template/vendors/flot.curvedlines/curvedLines.js",
				"~/Template/vendors/DateJS/build/date.js",
				"~/Template/vendors/jqvmap/dist/jquery.vmap.js",
				"~/Template/vendors/jqvmap/dist/maps/jquery.vmap.world.js",
				"~/Template/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js",
				"~/Template/vendors/moment/min/moment.min.js",
				"~/Template/vendors/bootstrap-daterangepicker/daterangepicker.js",
				"~/Template/vendors/datatables.net/js/jquery.dataTables.min.js",
				"~/Template/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js",
				"~/Template/vendors/datatables.net-buttons/js/dataTables.buttons.min.js",
				"~/Template/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js",
				"~/Template/vendors/datatables.net-buttons/js/buttons.flash.min.js",
				"~/Template/vendors/datatables.net-buttons/js/buttons.html5.min.js",
				"~/Template/vendors/datatables.net-buttons/js/buttons.print.min.js",
				"~/Template/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js",
				"~/Template/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js",
				"~/Template/vendors/datatables.net-responsive/js/dataTables.responsive.min.js",
				"~/Template/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js",
				"~/Template/vendors/datatables.net-scroller/js/dataTables.scroller.min.js",
				"~/Template/vendors/jszip/dist/jszip.min.js",
				"~/Template/vendors/pdfmake/build/pdfmake.min.js",
				"~/Template/vendors/pdfmake/build/vfs_fonts.js",
				"~/Template/build/js/custom.min.js",
				"~/Scripts/toastr.js",
				"~/Scripts/toastr.min.js"
				));
		}
	}
}
