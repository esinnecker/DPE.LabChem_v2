﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DPE.LabChem.Domain.Entities;
using DPE.LabChem.Infra.Data.ORM.EF;

namespace DPE.LabChem.Web.Controllers
{
    public class MuralController : Controller
    {
        private LabChemDbContext db = new LabChemDbContext();

        public ActionResult Credential()
		{
            return View("Credential", "_LoginLayout");
		}

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Credential(Mural mural)
		{
            if (ModelState != null)
			{
                mural.Subject += mural.Email + "\n\n Thank you!!";
				try
				{
                    db.Mural.Add(mural);
                    db.SaveChanges();
                    ViewBag.Success = "Credential Required Successfuly";
				}
                catch (Exception ex)
				{
                    ViewBag.Error = "Verify that you entered the data correctly";
                    ModelState.AddModelError("error_sendmail", ex.Message);
                    return View("Credential");
                }
                return RedirectToAction("Login", "Account");
			}
            return View(mural);
		}

        public ActionResult Register(int? id)
		{
            if (id == null)
			{
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}            
            return RedirectToAction("Create", "UserAdmin", new { id = id });
		}

        // GET: Mural
        public async Task<ActionResult> Index()
        {
            return View(await db.Mural.ToListAsync());
        }

        // GET: Mural/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mural mural = await db.Mural.FindAsync(id);
            if (mural == null)
            {
                return HttpNotFound();
            }
            return View(mural);
        }

        // GET: Mural/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Mural/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Date,Title,Subject,Email,User")] Mural mural)
        {
            if (ModelState.IsValid)
            {
                db.Mural.Add(mural);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(mural);
        }

        // GET: Mural/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mural mural = await db.Mural.FindAsync(id);
            if (mural == null)
            {
                return HttpNotFound();
            }
            return View(mural);
        }

        // POST: Mural/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Date,Title,Subject,Email,User")] Mural mural)
        {
            if (ModelState.IsValid)
            {
                db.Entry(mural).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(mural);
        }

        // GET: Mural/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mural mural = await db.Mural.FindAsync(id);
            if (mural == null)
            {
                return HttpNotFound();
            }
            return View(mural);
        }

        // POST: Mural/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Mural mural = await db.Mural.FindAsync(id);
            db.Mural.Remove(mural);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
