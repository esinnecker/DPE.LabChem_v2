﻿using DPE.LabChem.Domain.Entities;
using DPE.LabChem.Identity.Configuration;
using DPE.LabChem.Identity.Context;
using DPE.LabChem.Identity.Users;
using DPE.LabChem.Infra.Data.ORM.EF;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace DPE.LabChem.Web.Controllers.Account
{
	public class UserAdminController : Controller
    {
        private ApplicationUserManager _userManager;
        private ApplicationDbContext _dbContext;
		private LabChemDbContext db = new LabChemDbContext();

		public UserAdminController()
		{

		}

		public UserAdminController(ApplicationUserManager userManager)
		{
            this.UserManager = userManager;
		}

        public ApplicationUserManager UserManager
		{
			get
			{
				return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
			}
			set
			{
				_userManager = value;
			}
		}
        // GET: UserAdmin
        public async Task<ActionResult> Index()
        {
            return View(await UserManager.Users.ToListAsync());
        }

		#region Register
		public ActionResult Create(int? id)
		{
			if (id == null)
			{
				return View("Register", "_Layout");
			}
			else
			{
				Mural mural = db.Mural.Find(id);

				if (mural == null)
				{
					return HttpNotFound();
				}

				RegisterViewModel registerViewModel = new RegisterViewModel
				{
					UserName = mural.Email,
					FullName = mural.User,
					Email = mural.Email
				};

				return View("Register", "_Layout", registerViewModel);
			}
			
			//return RedirectToAction("Register", "UserAdmin", registerViewModel);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create(RegisterViewModel register)
		{
			if (ModelState.IsValid)
			{
				var user = new ApplicationUser
				{
					UserName = register.UserName,
					FullName = register.FullName,
					Email = register.Email,
					DateOfBirth = register.DateOfBirth,
					Skype = register.Skype,
					Bio = register.Bio
				};

				var result = UserManager.Create(user, register.Password);

				if (result.Succeeded)
				{
					return RedirectToAction("Index", "UserAdmin");
				}
				else
				{
					foreach (var error in result.Errors)
					{
						ModelState.AddModelError("", error);
					}									}
			}
			return View("Register", "_Layout");
		}
		#endregion

		// POST: UserAdmin/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> Delete(int id)
		{
			if (ModelState.IsValid)
			{
				if (id == null)
				{
					return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
				}

				var user = await UserManager.FindByIdAsync(id.ToString());
				if (user == null)
				{
					return HttpNotFound();
				}
				var result = await UserManager.DeleteAsync(user);
				if (!result.Succeeded)
				{
					ModelState.AddModelError("", result.Errors.First());
					return View();
				}
				return RedirectToAction("Index", "UserAdmin");
			}
			return View();
		}

	}
}