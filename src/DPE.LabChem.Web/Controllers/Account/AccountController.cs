﻿using DPE.LabChem.Identity.Configuration;
using DPE.LabChem.Identity.Context;
using DPE.LabChem.Identity.Users;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace DPE.LabChem.Web.Controllers.Account
{
	public class AccountController : Controller
    {
		private ApplicationUserManager _userManager;
		private ApplicationSigninManager _signInManager;		

		public AccountController()
		{
		}

		public ApplicationUserManager UserManager
		{
			get
			{
				return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
			}
			private set
			{
				_userManager = value;
			}
		}

		public ApplicationSigninManager SignInManager
		{
			get
			{
				return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSigninManager>();
			}
			private set
			{
				_signInManager = value;
			}
		}

		#region Login
		public ActionResult Login()
		{
			return View("Login", "_LoginLayout");
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> Login(LoginViewModel vModel, string returnUrl)
		{
			if (!ModelState.IsValid)
			{
				return View(vModel);
			}

			var result = await SignInManager.PasswordSignInAsync(vModel.UserName, vModel.Password, vModel.RememberMe, shouldLockout: false);

			switch (result)
			{
				case SignInStatus.Success:
					var identity = (ClaimsIdentity)User.Identity;
					AuthenticationManager.SignIn(new AuthenticationProperties
					{
						IsPersistent = vModel.RememberMe
					}, identity);

					return RedirectToAction("Index", "Mural");
				case SignInStatus.LockedOut:
					return View("LockOut");
				case SignInStatus.RequiresVerification:
					return RedirectToAction("SendCode", new { ReturnUrl = returnUrl });
				case SignInStatus.Failure:					
				default:
					ModelState.AddModelError("", "Invalid Login Attempt");
					return View("Login", "_LoginLayout");
			}						
		}


		#endregion

		#region Logout
		public ActionResult Logout()
        {
			var authManager = HttpContext.GetOwinContext().Authentication;
			authManager.SignOut();
			return RedirectToAction("Login", "Account");
        }
		#endregion

		#region Helpers
		// Used for XSRF protection when adding external logins
		private const string XsrfKey = "XsrfId";

		private IAuthenticationManager AuthenticationManager
		{
			get
			{
				return HttpContext.GetOwinContext().Authentication;
			}
		}

		private void AddErrors(IdentityResult result)
		{
			foreach (var error in result.Errors)
			{
				ModelState.AddModelError("", error);
			}
		}

		private ActionResult RedirectToLocal(string returnUrl)
		{
			if (Url.IsLocalUrl(returnUrl))
			{
				return Redirect(returnUrl);
			}
			return RedirectToAction("Index", "Home");
		}

		internal class ChallengeResult : HttpUnauthorizedResult
		{
			public ChallengeResult(string provider, string redirectUri)
				: this(provider, redirectUri, null)
			{
			}

			public ChallengeResult(string provider, string redirectUri, string userId)
			{
				LoginProvider = provider;
				RedirectUri = redirectUri;
				UserId = userId;
			}

			public string LoginProvider { get; set; }
			public string RedirectUri { get; set; }
			public string UserId { get; set; }

			public override void ExecuteResult(ControllerContext context)
			{
				var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
				if (UserId != null)
				{
					properties.Dictionary[XsrfKey] = UserId;
				}
				context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
			}
		}
		#endregion
	}
}