﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DPE.LabChem.Domain.Entities;
using DPE.LabChem.Infra.Data.ORM.EF;

namespace DPE.LabChem.Web.Controllers
{
    public class CountryController : Controller
    {
        private LabChemDbContext db = new LabChemDbContext();

        // GET: Country
        public async Task<ActionResult> Index()
        {
            return View(await db.Country.ToListAsync());
        }

        // GET: Country/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)  
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Country country = await db.Country.FindAsync(id);
            if (country == null)
            {
                return HttpNotFound();
            }
            return PartialView(country);
        }
        public ActionResult Cancel()
		{
			return RedirectToAction("Index");
		}

        // GET: Country/Create
        public ActionResult Create()
        {
            return PartialView();
        }

        // POST: Country/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Initials,Name")] Country country)
        {
            if (ModelState.IsValid)
            {
                db.Country.Add(country);
                await db.SaveChangesAsync();
                return Json(new { success = true, message = "Country created successfully" }, JsonRequestBehavior.AllowGet);
            }
            else 
            {
                //IEnumerable<ModelError> errors = ModelState.Values.SelectMany(items => items.Errors).;

                //var errors = ModelState.Values.SelectMany(m => m.Errors).Select(e => e.ErrorMessage).ToList();

                var errors = string.Join("<br>", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage));   // html break instead of " | "

                return Json(new { success = false, message = errors }, JsonRequestBehavior.AllowGet);
            }            
        }

        // GET: Country/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Country country = await db.Country.FindAsync(id);
            if (country == null)
            {
                return HttpNotFound();
            }
            return PartialView(country);
        }

        // POST: Country/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Initials,Name")] Country country)
        {
            if (ModelState.IsValid)
            {
                db.Entry(country).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return Json(new { success = true, message = "Country updated successfully" }, JsonRequestBehavior.AllowGet);
                //return RedirectToAction("Index");
            }
            else
            {
                //IEnumerable<ModelError> errors = ModelState.Values.SelectMany(items => items.Errors).;

                //var errors = ModelState.Values.SelectMany(m => m.Errors).Select(e => e.ErrorMessage).ToList();

                var errors = string.Join("<br>", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage));   // html break instead of " | "

                return Json(new { success = false, message = errors }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: Country/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Country country = await db.Country.FindAsync(id);
            if (country == null)
            {
                return HttpNotFound();
            }
            return PartialView(country);
        }

        // POST: Country/DeleteCountry
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public async Task<ActionResult> DeleteCountry(int id)
        {
            if (ModelState.IsValid)
            {
                Country country = await db.Country.FindAsync(id);
                db.Country.Remove(country);
                await db.SaveChangesAsync();
                return Json(new { success = true, message = "Country deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                //IEnumerable<ModelError> errors = ModelState.Values.SelectMany(items => items.Errors).;

                var errors = ModelState.Values.SelectMany(m => m.Errors).Select(e => e.ErrorMessage).ToList();

                return Json(new { success = false, message = errors }, JsonRequestBehavior.AllowGet);
            }
        }

        // POST: Country/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            if (ModelState.IsValid)
			{
                Country country = await db.Country.FindAsync(id);
                db.Country.Remove(country);
                await db.SaveChangesAsync();
                return Json(new { success = true, message = "Country deleted successfully" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                //IEnumerable<ModelError> errors = ModelState.Values.SelectMany(items => items.Errors).;

                var errors = ModelState.Values.SelectMany(m => m.Errors).Select(e => e.ErrorMessage).ToList();

                return Json(new { success = false, message = errors }, JsonRequestBehavior.AllowGet);
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }        
    }
}
