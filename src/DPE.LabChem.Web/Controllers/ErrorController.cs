﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DPE.LabChem.Web.Controllers
{
    public class ErrorController : Controller
    {

        [HttpGet]
        public ActionResult Index()
        {
            TempData["error"] = "Error Occurred!";
            return View();
        }



        public ActionResult NotFound()
        {
            TempData["error"] = "Page not Found";
            return View();
            //return View("NotFound", "_LoginLayout");
        }

        [HttpGet]
        public ActionResult AccessDenied()
        {
            TempData["error"] = "Access Denied";
            return View("Index");
        }

        [HttpGet]
        public ActionResult InternalServerError()
        {
            TempData["error"] = "Internal Server Error";
            return View("Index");
        }
    }
}