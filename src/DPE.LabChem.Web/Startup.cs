﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using DPE.LabChem.Web;
using Microsoft.Owin.Security.Cookies;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using DPE.LabChem.Identity.Configuration;
using DPE.LabChem.Identity.Users;
using DPE.LabChem.Identity.Context;

[assembly: OwinStartup(typeof(Startup))]

namespace DPE.LabChem.Web
{
	public class Startup
	{
		public void Configuration(IAppBuilder app)
		{			
			app.CreatePerOwinContext(ApplicationDbContext.Create);
			app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
			app.CreatePerOwinContext<ApplicationSigninManager>(ApplicationSigninManager.Create);
			app.CreatePerOwinContext<ApplicationRoleManager>(ApplicationRoleManager.Create);

			app.UseCookieAuthentication(new CookieAuthenticationOptions
			{
				AuthenticationMode = Microsoft.Owin.Security.AuthenticationMode.Active,
				AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
				LoginPath = new PathString("/Account/Login"),
				LogoutPath = new PathString("/Account/Logout"),
				Provider = new CookieAuthenticationProvider
				{
					// Enables the application to validate the security stamp when the user logs in.
					// This is a security feature which is used when you change a password or add an external login to your account.  
					OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<ApplicationUserManager, ApplicationUser>(
						validateInterval: TimeSpan.FromMinutes(180),
						regenerateIdentity: (manager, user) => user.GenerateUserIdentityAsync(manager))
				}
			});
		}
	}
}
